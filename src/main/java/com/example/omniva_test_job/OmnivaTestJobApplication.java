package com.example.omniva_test_job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ConfigurationPropertiesScan("com.example.omniva_test_job")
public class OmnivaTestJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(OmnivaTestJobApplication.class, args);
    }

}
