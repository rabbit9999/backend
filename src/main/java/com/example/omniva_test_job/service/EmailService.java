package com.example.omniva_test_job.service;

import com.example.omniva_test_job.config.Config;
import com.example.omniva_test_job.entity.EmailEntity;
import com.example.omniva_test_job.repository.EmailEntityRepository;
import com.example.omniva_test_job.restapi.EmailDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class EmailService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String MSG_NO_VALID_DATA = "No valid email data present";

    @Autowired
    private EmailEntityRepository emailEntityRepository;

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    Config config;

    public void checkUnsentEmails() {
        List<EmailEntity> emailList = emailEntityRepository.findByCreatedAtIsLessThanAndSentIsFalse(getTargetDate());

        emailList.forEach(email -> {
            try {
                sendEmail(email);
                email.markAsSent();
                emailEntityRepository.save(email);
                logger.info("Email sent: {}", email);
            } catch (Exception e) {
                logger.error("Failed to send email: {} {}", email, e);
            }
        });
    }

    private Date getTargetDate() {
        Calendar date = Calendar.getInstance();
        long timeInSecs = date.getTimeInMillis();
        return new Date(timeInSecs - (config.getDelay() * 60 * 1000));
    }

    private void sendEmail(EmailEntity email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(config.getFrom());
        message.setTo(email.getAddress());
        message.setSubject(email.getSubject());
        message.setText(email.getMessage());
        emailSender.send(message);
    }

    public AddEmailResult addEmailToQueue(EmailDto email) {
        if(email == null || !email.validate()) {
            logger.warn(MSG_NO_VALID_DATA);
            return AddEmailResult.builder()
                    .errorMessage(MSG_NO_VALID_DATA)
                    .isError(true)
                    .build();
        }

        EmailEntity savedEmail = emailEntityRepository.save(EmailEntity.builder()
                .address(email.getAddress())
                .subject(email.getSubject())
                .message(email.getMessage())
                .build());

        logger.info("Email saved: {}", savedEmail);
        return AddEmailResult.builder()
                .entity(savedEmail)
                .build();
    }

}
