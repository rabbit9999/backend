### To correct work this application you should configure database connection and email server by followed properties

#### Database
* spring.datasource.url=
* spring.datasource.username=
* spring.datasource.password=

#### Email server
* spring.mail.host=
* spring.mail.port=
* spring.mail.username=
* spring.mail.password=

#### Noreply email
omniva.from=


### You can tune sending parameters by followed props
* omniva.rate=5000 (frequency to check DB in millis)
* omniva.delay=60 (delay before send email in minutes)