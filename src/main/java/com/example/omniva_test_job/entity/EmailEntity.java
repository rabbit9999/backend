package com.example.omniva_test_job.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class EmailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String address;
    private String subject;
    private String message;
    private Date createdAt;
    private Date sentAt;
    private boolean sent;

    @PrePersist
    protected void onCreate() {
        createdAt = new Date();
        sent = false;
    }

    public void markAsSent() {
        if(!sent){
            sentAt = new Date();
            sent = true;
        }
    }

    @Override
    public String toString() {
        return "\nid: " + id +
        "\naddress: " + address +
        "\nsubject: " + subject +
        "\nmessage: " + message +
        "\ncreatedAt: " + createdAt +
        "\nsentAt: " + sentAt +
        "\nsent: " + sent;
    }
}
