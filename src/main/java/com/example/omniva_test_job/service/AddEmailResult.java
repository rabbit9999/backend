package com.example.omniva_test_job.service;

import com.example.omniva_test_job.entity.EmailEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder
public class AddEmailResult {
    private EmailEntity entity;
    private boolean isError;
    private String errorMessage;
}
