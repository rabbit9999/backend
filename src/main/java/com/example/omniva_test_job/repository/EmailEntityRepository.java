package com.example.omniva_test_job.repository;

import com.example.omniva_test_job.entity.EmailEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface EmailEntityRepository extends CrudRepository<EmailEntity, Integer> {
    List<EmailEntity> findByCreatedAtIsLessThanAndSentIsFalse(Date date);
}
