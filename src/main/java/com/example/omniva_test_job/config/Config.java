package com.example.omniva_test_job.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Setter
@Getter
@ConfigurationProperties(prefix = "omniva")
public class Config {
    private Long rate = 5000L;
    private Integer delay = 60;
    private String from = "noreply@email.net";
}
