package com.example.omniva_test_job.restapi;

import com.example.omniva_test_job.service.AddEmailResult;
import com.example.omniva_test_job.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RestApiController {

    public static final String SUCCESS_MSG = "Email has been added to queue";

    @Autowired
    private EmailService emailService;

    @CrossOrigin(origins = "*")
    @PostMapping("/addEmail")
    public RestApiResponse addEmailToQueue(@RequestBody EmailDto email) {

        AddEmailResult result = emailService.addEmailToQueue(email);

        if(result.isError()){
            return RestApiResponse.builder()
                    .hasError(true)
                    .message(result.getErrorMessage())
                    .build();
        } else {
            return RestApiResponse.builder()
                    .hasError(false)
                    .message(SUCCESS_MSG)
                    .build();
        }
    }
}