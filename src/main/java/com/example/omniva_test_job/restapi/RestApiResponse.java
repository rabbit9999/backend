package com.example.omniva_test_job.restapi;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder
public class RestApiResponse {
    private boolean hasError;
    private String message;
}
