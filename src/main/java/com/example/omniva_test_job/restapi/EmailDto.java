package com.example.omniva_test_job.restapi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class EmailDto {
    private String address;
    private String subject;
    private String message;

    public boolean validate() {
        return StringUtils.hasText(address) &&
                StringUtils.hasText(subject) &&
                StringUtils.hasText(message);
    }
}
