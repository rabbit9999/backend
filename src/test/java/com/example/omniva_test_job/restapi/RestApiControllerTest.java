package com.example.omniva_test_job.restapi;

import com.example.omniva_test_job.entity.EmailEntity;
import com.example.omniva_test_job.repository.EmailEntityRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class RestApiControllerTest {

    @Autowired
    RestApiController restApiController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmailEntityRepository emailEntityRepository;

    @Test
    public void addEmailTest() throws Exception{
        assertNotNull(restApiController);

        Mockito.when(emailEntityRepository.save(Mockito.any(EmailEntity.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        this.mockMvc.perform(post("/api/addEmail")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"address\": \"email\", \"subject\": \"subject\", \"message\": \"message\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.hasError").value(false))
                .andExpect(jsonPath("$.message").value(RestApiController.SUCCESS_MSG));

        this.mockMvc.perform(post("/api/addEmail")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.hasError").value(true))
                .andExpect(jsonPath("$.message").isNotEmpty());
    }
}