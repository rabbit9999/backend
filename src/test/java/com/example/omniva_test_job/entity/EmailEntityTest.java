package com.example.omniva_test_job.entity;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EmailEntityTest {

    @Test
    public void markAsSentTest() {
        EmailEntity emailEntity = new EmailEntity();

        assertNull(emailEntity.getSentAt());
        assertFalse(emailEntity.isSent());

        emailEntity.markAsSent();

        assertNotNull(emailEntity.getSentAt());
        assertTrue(emailEntity.isSent());
    }

}