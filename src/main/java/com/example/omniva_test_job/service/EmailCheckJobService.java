package com.example.omniva_test_job.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class EmailCheckJobService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private EmailService emailService;

    @Scheduled(fixedRateString = "${omniva.rate}")
    private void checkUnsentEmailsJob() {
        logger.debug("Start email checking job...");
        emailService.checkUnsentEmails();
    }
}
