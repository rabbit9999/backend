package com.example.omniva_test_job.service;

import com.example.omniva_test_job.entity.EmailEntity;
import com.example.omniva_test_job.repository.EmailEntityRepository;
import com.example.omniva_test_job.restapi.EmailDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmailServiceTest {

    @Autowired
    private EmailService emailService;

    @MockBean
    private EmailEntityRepository emailEntityRepository;

    @BeforeEach
    public void init() {
        Mockito.when(emailEntityRepository.save(Mockito.any(EmailEntity.class)))
                .thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    public void addEmailToQueueSuccessTest() {
        EmailDto emailDto = EmailDto.builder()
                .address("address")
                .subject("subj")
                .message("mes")
                .build();

        assertNotNull(emailService);

        AddEmailResult result = emailService.addEmailToQueue(emailDto);

        assertNotNull(result);
        assertFalse(result.isError());

        EmailEntity emailEntity = result.getEntity();

        assertNotNull(emailEntity);
        assertEquals(emailEntity.getAddress(), "address");
        assertEquals(emailEntity.getSubject(), "subj");
        assertEquals(emailEntity.getMessage(), "mes");
    }

    @Test
    public void addEmailToQueueFailTest() {
        EmailDto emailDto = EmailDto.builder().build();
        assertNotNull(emailService);

        AddEmailResult result = emailService.addEmailToQueue(emailDto);

        assertNotNull(result);
        assertTrue(result.isError());

        EmailEntity emailEntity = result.getEntity();
        assertNull(emailEntity);
    }
}