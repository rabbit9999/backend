package com.example.omniva_test_job.restapi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EmailDtoTest {

    @Test
    public void dtoValidationFailTest() {
        EmailDto dto = new EmailDto();
        assertFalse(dto.validate());

        dto = EmailDto.builder().message("mes").build();
        assertFalse(dto.validate());

        dto = EmailDto.builder().subject("subj").build();
        assertFalse(dto.validate());

        dto = EmailDto.builder().address("qwe").build();
        assertFalse(dto.validate());
    }

    @Test
    public void dtoValidationSuccessTest() {
        EmailDto dto = EmailDto.builder().address("qwe").subject("subj").message("mes").build();
        assertTrue(dto.validate());
    }

}